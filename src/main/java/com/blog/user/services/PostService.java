package com.blog.user.services;

import com.blog.user.dto.PostDTO;
import com.blog.user.model.Post;

/**
 * Created by user on 20/7/17.
 */
public interface PostService {

    Post create(PostDTO postDTO);
}
