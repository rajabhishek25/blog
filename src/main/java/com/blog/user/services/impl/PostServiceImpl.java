package com.blog.user.services.impl;

import com.blog.user.converters.PostConverter;
import com.blog.user.dto.PostDTO;
import com.blog.user.model.Post;
import com.blog.user.repositories.PostRepository;
import com.blog.user.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by user on 20/7/17.
 */
@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostConverter postConverter;

    @Autowired
    private PostRepository postRepository;

    @Override
    public Post create(PostDTO postDTO) {
        Post savedPost = postRepository.save(postConverter.convertTo(postDTO));
        return savedPost;
    }
}
