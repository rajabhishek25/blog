package com.blog.user.model;


import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 20/7/17.
 */
@Getter
@Setter
@Document
public class Post implements Serializable {

    @Id
    private String id;

    private String name;

    private String content;

    private Date date;

    private List<String> keywords;

}
