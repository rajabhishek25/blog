package com.blog.user.controllers;

import com.blog.user.dto.PostDTO;
import com.blog.user.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by user on 20/7/17.
 */
@RestController
@RequestMapping(value = "/post")
public class BlogPostController {

    @Autowired
    private PostService postService;

    @RequestMapping(value = "/" , method = RequestMethod.POST)
    public void createPost(@RequestBody PostDTO postDTO){
        postService.create(postDTO);
    }
}
