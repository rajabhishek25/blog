package com.blog.user.repositories;

import com.blog.user.model.Post;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by user on 20/7/17.
 */
public interface PostRepository extends MongoRepository<Post, Long> {
}
