package com.blog.user.converters;

import com.blog.user.dto.PostDTO;
import com.blog.user.model.Post;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by user on 20/7/17.
 */
@Component
public class PostConverter {

    public Post convertTo (PostDTO postDTO){
        Post post= new Post();
        post.setContent(postDTO.getContent());
        post.setDate(new Date(postDTO.getDate()));
        post.setName(postDTO.getName());
        return post;
    }
}
