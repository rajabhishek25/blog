package com.blog.user.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by user on 20/7/17.
 */
@Getter
@Setter
@ToString
public class PostDTO implements Serializable{

    private String id;
    private String name;
    private String content;
    private long date;
}
